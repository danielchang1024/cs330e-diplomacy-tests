#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, get_support, get_destination

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        l = diplomacy_read("A Madrid Hold\nB Barcelona Hold\nC London Hold\n")
        self.assertEqual(l, [("A", "Madrid", "Hold"), ("B", "Barcelona", "Hold"), ("C", "London", "Hold")])

    def test_read_2(self):
        l = diplomacy_read("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        self.assertEqual(l, [("A", "Madrid", "Hold"), ("B", "Barcelona", "Move Madrid"), ("C", "London", "Support B")])

    def test_read_3(self):
        l = diplomacy_read("A Madrid Move Paris\nB Barcelona Support D\nC London Move Paris\nD Paris Move Austin\nE Austin Support C\n")
        self.assertEqual(l, [("A", "Madrid", "Move Paris"), ("B", "Barcelona", "Support D"), ("C", "London", "Move Paris"), ("D", "Paris", "Move Austin"), ("E", "Austin", "Support C")])

    def test_read_4(self):
        l = diplomacy_read("A Madrid Hold\nB London Support A\nC Austin Move Madrid\nD Barcelona Support C\n")
        self.assertEqual(l, [("A", "Madrid", "Hold"), ("B", "London", "Support A"), ("C", "Austin", "Move Madrid"), ("D", "Barcelona", "Support C")])

    def test_read_5(self):
        self.assertRaises(AssertionError, diplomacy_read, "")

    # ----
    # Support
    # ----

    def test_support_1(self):
        d = get_support([("A", "Paris", "Hold"), ("B", "Barcelona", "Support A"), ("C", "London", "Support B")])
        self.assertEqual(d, {"A": 1, "B": 1, "C": 0})
    
    def test_support_2(self):
        d = get_support([("A", "Paris", "Hold"), ("B", "Barcelona", "Support A"), ("C", "London", "Support B"), ("D", "Austin", "Support C")])
        self.assertEqual(d, {"A": 1, "B": 1, "C": 1, "D": 0})

    def test_support_3(self):
        d = get_support([("A", "Paris", "Hold"), ("B", "Barcelona", "Support A"), ("C", "London", "Support B"), ("D", "Austin", "Support C"), ("E", "Waco", "Move Barcelona"), ("F", "Berlin", "Move London")])
        self.assertEqual(d, {"A": 0, "B": 0, "C": 1, "D": 0, "E": 0, "F": 0})

    def test_support_4(self):
        d = get_support([("A", "Austin", "Hold"), ("B", "Paris", "Support A"), ("C", "London", "Support A"), ("D", "Barcelona", "Move Paris"), ("E", "Berlin", "Support C")])
        self.assertEqual(d, {"A": 1, "B": 0, "C": 1, "D": 0, "E": 0})

    def test_support_5(self):
        d = get_support([("A", "Berlin", "Hold"), ("B", "Barcelona", "Support A"), ("C", "London", "Support A"), ("D", "Paris", "Support B"), ("E", "Austin", "Support A")])
        self.assertEqual(d, {"A": 3, "B": 1, "C": 0, "D": 0, "E": 0})


    # ----
    # Destinations
    # ----

    def test_destination_1(self):
        d = get_destination([("A", "Berlin", "Hold"), ("B", "Barcelona", "Support A"), ("C", "London", "Support A"), ("D", "Paris", "Support B"), ("E", "Austin", "Support A")])
        self.assertEqual(d, {"Berlin": ["A"], "Barcelona": ["B"], "London": ["C"], "Paris": ["D"],"Austin": ["E"]})

    def test_destination_2(self):
        d = get_destination([("A", "Berlin", "Hold"), ("B", "Barcelona", "Move Berlin"), ("C", "London", "Move Berlin"), ("D", "Paris", "Support C"), ("E", "Austin", "Support C")])
        self.assertEqual(d, {"Berlin": ["A", "B", "C"], "Paris": ["D"],"Austin": ["E"]})

    def test_destination_3(self):
        d = get_destination([("A", "Berlin", "Support B"), ("B", "Barcelona", "Hold"), ("C", "London", "Move Berlin"), ("D", "Paris", "Support B"), ("E", "Austin", "Move Paris")])
        self.assertEqual(d, {"Berlin": ["A", "C"], "Barcelona": ["B"], "Paris": ["D", "E"]})

    def test_destination_4(self):
        d = get_destination([("A", "Paris", "Hold"), ("B", "Barcelona", "Support A"), ("C", "London", "Support B"), ("D", "Austin", "Support C"), ("E", "Berlin", "Move Barcelona"), ("F", "Madrid", "Move London")])
        self.assertEqual(d, {"Paris": ["A"], "Barcelona": ["B", "E"], "London": ["C", "F"], "Austin": ["D"]})

    def test_destination_5(self):
        d = get_destination([("A", "Austin", "Hold"), ("B", "Paris", "Support A"), ("C", "London", "Support A"), ("D", "Barcelona", "Move Paris"), ("E", "Berlin", "Support C")])
        self.assertEqual(d, {"Austin": ["A"], "Paris": ["B", "D"], "London": ["C"], "Berlin": ["E"]})

    def test_destination_6(self):
        d = get_destination([("A", "Berlin", "Move Madrid"), ("B", "Barcelona", "Move Berlin"), ("C", "London", "Move Madrid")])
        self.assertEqual(d, {"Madrid": ["A", "C"], "Berlin": ["B"]})

    # ----
    # eval
    # ----

    def test_eval_1(self):
        l = diplomacy_eval([("A", "Paris", "Hold"), ("B", "Barcelona", "Hold"), ("C", "London", "Hold")])
        self.assertEqual(l, [("A", "Paris"), ("B", "Barcelona"), ("C", "London")])
    
    def test_eval_2(self):
        l = diplomacy_eval([("A", "Madrid", "Move Barcelona"), ("B", "Barcelona", "Move London"), ("C", "London", "Move Madrid")])
        self.assertEqual(l, [("A", "Barcelona"), ("B", "London"), ("C", "Madrid")])

    def test_eval_3(self):
        l = diplomacy_eval([("A", "Austin", "Hold"), ("B", "Paris", "Move Austin"), ("C", "London", "Support B")])
        self.assertEqual(l, [("A", "[dead]"), ("B", "Austin"), ("C", "London")])

    def test_eval_4(self):
        l = diplomacy_eval([("A", "Madrid", "Hold"), ("B", "Barcelona", "Move Madrid"), ("C", "London", "Support A"), ("D", "Paris", "Move London"), ("E", "Austin", "Support C")])
        self.assertEqual(l, [("A", "[dead]"), ("B", "[dead]"), ("C", "London"), ("D", "[dead]"), ("E", "Austin")])

    def test_eval_5(self):
        l = diplomacy_eval([("A", "Austin", "Hold"), ("B", "Barcelona", "Move Austin"), ("C", "London", "Support A"), ("D", "Paris", "Support B"),])
        self.assertEqual(l, [("A", "[dead]"), ("B", "[dead]"), ("C", "London"), ("D", "Paris")])

    def test_eval_6(self):
        l = diplomacy_eval([("A", "Austin", "Hold"), ("B", "Barcelona", "Move Austin"), ("C", "London", "Support B"), ("D", "Paris", "Support C"), ("E", "Berlin", "Support A")])
        self.assertEqual(l, [("A", "[dead]"), ("B", "[dead]"), ("C", "London"), ("D", "Paris"), ("E", "Berlin")])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [("A", "Madrid"), ("B", "[dead]")])
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [("A", "Madrid"), ("B", "Barcelona"), ("C", "Paris")])
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC Paris\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [("A", "[dead]"), ("B", "[dead]"), ("C", "[dead]"), ("D", "[dead]")])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_print_4(self):
        w = StringIO()
        diplomacy_print(w, [("A", "Madrid"), ("B", "[dead]"), ("C", "Austin"), ("D", "[dead]")])
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC Austin\nD [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Paris Hold\nB Barcelona Hold\nC London Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB Barcelona\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move London\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB London\nC Madrid\n")

    def test_solve_3(self):
        r = StringIO("A Austin Hold\nB Paris Move Austin\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Austin\nC London\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Paris Move London\nE Austin Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE Austin\n")

    def test_solve_5(self):
        r = StringIO("A Austin Hold\nB Barcelona Move Austin\nC London Support A\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD Paris\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_8(self):
        r = StringIO("A Austin Hold\nB Paris Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
"""
